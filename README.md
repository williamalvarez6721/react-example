# React example for Chrysalis Solmotive and some findings on their key2key React app:

## Click <a href='https://react-app-william-example.herokuapp.com/'>here</a> to check the app live

</br>

#### Objective:
To demonstrate William's ability to create a React app. Using similar tools and techniques as the key2key website. Moreover to give some friendly comments on some of the code.

#### Dependencies:
Styling: Mui's core.
```
@mui/icons-material
@mui/material
@mui/styled-engine-sc
```

## Friendly comments:
#### General
- In the case the company wants to hide the source code: a quick fix is to set an environment variable when deploying => `GENERATE_SOURCEMAP=false`
- For SEO purposes: the `index.html` description has the React template on it.
- Branding: The React logo is still the main favicon: If the company want they can change it in the `index.html`


<br/>

#### Header:
- In the case the company wants to use react-router-dom 6 or higher in the near future: `useHistory` is not supported in new React dom versions, this has been replaced by `useNavigate`. 
