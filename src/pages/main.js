import React from "react";
import logo from '../logo.svg';
import ProductCard from "../cards/product_main";

// This is the org dashboard.

const MainPage = () => {


  return(
      <header className="main_container">

        <p>
          This is the main page
        </p>
        <ProductCard/>
      </header>
  )

}

export default MainPage