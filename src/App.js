import React from 'react';
import { BrowserRouter } from 'react-router-dom';


//Styles
import './styles/header.scss'
import './styles/footer.scss'
import './styles/main_page.scss'



//Components
import Header from './components/header'
import Footer from './components/footer'

//Pages
import MainPage from './pages/main';


function App() {
  return (
    <BrowserRouter>
    <Header />
    <MainPage />
    <Footer />
    </BrowserRouter>
  
  );
}

export default App;
