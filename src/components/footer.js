import React from "react";
import Box from "@mui/material/Box";
import Typography from '@mui/material/Typography'



const Footer = () => {
return (<footer id="site-footer">

<section className="horizontal-footer-section" id="footer-middle-section">
    <div id="footer-about" className="footer-columns footer-columns-large">
        <h1>Our Address</h1>
        <address>
            <p><img alt='' src="https://img.icons8.com/ios-filled/14/eeeeee/marker.png" />123, Nice Building, Birmingham</p>
            <p><img alt='' src="https://img.icons8.com/ios-filled/14/eeeeee/phone.png" />02 123 123123</p>
            <p><img  alt=''src="https://img.icons8.com/ios-filled/14/eeeeee/mail.png" />email@email.com</p>
        </address> 
    </div>
    <div className="footer-columns">
        <h1>Resources</h1>
        <ul className="footer-column-menu" role="menu">
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">FAQ</a>
            </li>
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">Media</a>
            </li>
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">Guides</a>
            </li>
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">Free Resources</a>
            </li>
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">Testimonials</a>
            </li>
        </ul>
    </div>
    <div className="footer-columns">
        <h1>Information</h1>
        <ul className="footer-column-menu" role="menu">
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">About Us</a>
            </li>
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">Terms of Use</a>
            </li>
            <li className="footer-column-menu-item">
                <a href="#" className="footer-column-menu-item-link" role="menuitem">Legal Information</a>
            </li>
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">Message Us</a>
            </li>
            <li className="footer-column-menu-item" role="menuitem">
                <a href="#" className="footer-column-menu-item-link">Leave a Feedback</a>
            </li>
        </ul>
    </div>
</section>

<section className="horizontal-footer-section" id="footer-bottom-section">
    <div id="footer-copyright-info">
        &copy; Amazing App. 2022. All rights reserved.
    </div>
    <div id="footer-social-buttons">
        <img alt='a' src="https://img.icons8.com/ios-filled/25/999999/facebook--v1.png"/>
        <img alt='a' src="https://img.icons8.com/ios-filled/25/999999/telegram-app.png"/>
        <img alt='a' src="https://img.icons8.com/ios-filled/25/999999/pinterest--v1.png"/>
        <img alt='a' src="https://img.icons8.com/ios-filled/25/999999/instagram--v1.png"/>
    </div>
</section>

</footer>)


};

export default Footer;